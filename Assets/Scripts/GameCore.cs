using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameCore : MonoBehaviour
{

    public static PlayerController Player { get; private set; }
    public static LevelManager LevelManager { get; private set; }

        
    // Start is called before the first frame update
    private void Awake()
    {
        Player = FindObjectOfType<PlayerController>();
        LevelManager = GetComponent<LevelManager>();
    }

}
