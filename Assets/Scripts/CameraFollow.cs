using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    private Camera cam;
    [SerializeField] Vector3 offset;

    [Range(1, 10)]
    [SerializeField] float smoothFactor;

    private void Awake()
    {
        cam = Camera.main;
    }

    private void LateUpdate()
    {
        Vector2 desiredPosition = transform.position + offset;
        Vector2 smoothedPosition = Vector2.Lerp(cam.transform.position, desiredPosition, smoothFactor * Time.deltaTime);
        cam.transform.position = new Vector3(smoothedPosition.x, smoothedPosition.y, -10f);
    }

}
