using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Rigidbody2D rb;

    [Header("Constants")]
    [SerializeField] float jumpForce;
    [SerializeField] float moveSpeed;

    [Header("Ground Detection")]
    [SerializeField] Transform bottomOfFeet;
    [SerializeField] float checkRadius;
    [SerializeField] LayerMask groundMask;

    private Vector2 movementInput;
    private bool facingRight = true;

    private bool isGrounded = false;

    private bool holdingMove = false;
    private bool moveInputChanged = false;

    private void FixedUpdate()
    {
        if(holdingMove)
        {
            rb.velocity = new Vector2(movementInput.x * moveSpeed, rb.velocity.y);
        }

        isGrounded = Physics2D.OverlapCircle(bottomOfFeet.position, checkRadius, groundMask);
    }

    public void OnMoveInput(InputAction.CallbackContext ctx)
    {
        //StopAllCoroutines();

        movementInput = ctx.ReadValue<Vector2>().normalized;

        //if (ctx.performed)
        //    StartCoroutine(Move());

        holdingMove = ctx.performed || ctx.started;

        if (movementInput.x > 0 && !facingRight)
            Flip();
        else if (movementInput.x < 0f && facingRight)
            Flip();

        

    }

    public void OnJumpInput(InputAction.CallbackContext ctx)
    {
        if(ctx.performed && isGrounded)
            rb.AddForce(Vector3.up * jumpForce);
    }

    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    private IEnumerator Move()
    {
        while (true)
        {
            rb.velocity = new Vector2(movementInput.x * moveSpeed, rb.velocity.y);

            yield return new WaitForFixedUpdate();
        }
    }
}
