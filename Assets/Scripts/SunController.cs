using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SunController : MonoBehaviour
{
    [SerializeField] GameObject sun;
    [SerializeField] InputActionReference activateSunMovement;

    [Range(1, 10)]
    [SerializeField] float smoothFactor;

    Coroutine moveSunRoutine;
    private Vector3 endMousePositionWorld;

    private void Awake()
    {
        activateSunMovement.action.performed += SunMovementActivated;
        activateSunMovement.action.canceled += SunMovementCanceled;
    }

    private void SunMovementActivated(InputAction.CallbackContext obj)
    {
        moveSunRoutine = StartCoroutine(MoveSun());
    }
    
    private void SunMovementCanceled(InputAction.CallbackContext obj)
    {
        endMousePositionWorld = new Vector3(Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()).x, Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue()).y, 10f);
        StopCoroutine(moveSunRoutine);

        StartCoroutine(MoveToEnd());
    }

   

    IEnumerator MoveSun()
    {
        // something like...
        while(true)
        {
            Vector3 mousePositionWorld = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());

            Vector3 desiredPosition = new Vector3(mousePositionWorld.x, mousePositionWorld.y, 10f);
            Vector3 smoothedPostion = Vector3.Lerp(sun.transform.position, desiredPosition, smoothFactor / 5f * Time.deltaTime);
            sun.transform.position = smoothedPostion;

            yield return null;
        }
        
    }

    IEnumerator MoveToEnd()
    {
        float dist = Vector3.Distance(endMousePositionWorld, sun.transform.position);

        Debug.Log("Dist: " + dist);
        Debug.Log("While: " + dist + "< 0.1f &&" + dist + " > -0.1f: | " + (dist < 0.1f) + " && " + (dist > -0.1f));

        while (!(dist < 0.01f && dist > -0.01f))
        {
            Debug.Log("moving Sun");
            Vector3 desiredPosition = new Vector3(endMousePositionWorld.x, endMousePositionWorld.y, 10f);
            Vector3 smoothedPostion = Vector3.Lerp(sun.transform.position, desiredPosition, smoothFactor / 5f * Time.deltaTime);
            sun.transform.position = smoothedPostion;

            dist = Vector3.Distance(endMousePositionWorld, sun.transform.position);
            Debug.Log("Dist: " + dist);
            yield return null;
        }

        yield return null;
    }

}
